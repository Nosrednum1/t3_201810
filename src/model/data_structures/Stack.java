package model.data_structures;

/**
 * @author Anderson Barragan Agudelo
 * @param <T> Dato gen�rico
 */
public class Stack<T extends Comparable<T>> implements IStack<T>{

	private Node<T> top;

	@Override
	public void push(T item) {
		Node<T> n = new Node<T>(item);
		if(top == null)
			top = n;
		else{
			n.setNext(top);
			top = n;}}

	@Override
	public T pop() {
		T pop = null;
		if(top != null){
			pop = top.getItem();
			top = top.next();}
		return pop;}

	@Override
	public boolean isEmpty() {return (top != null);}

}
