package model.vo;

public class Creator {

	private String trip_id;

	private String taxi_id;

	private String trip_start_timestamp;

	private double trip_miles;

	private double trip_total;

	private int trip_seconds;

	private String company;
}
