package view;

import java.util.Scanner;

import controller.Controller;
import model.vo.Taxi;
import model.vo.Service;

public class TaxiTripsManagerView {
	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		boolean fin=false;
		while(!fin)
		{
			printMenu();

			int option = sc.nextInt();

			switch(option)
			{
			case 1:
				System.out.println("Ingrese el Id del taxi para la carga de datos del subconjunto de datos small de servicios:");
				System.out.println("ejemplos de ids... " + "\n" +
						"85c39e068db414d181c6252a89fd822afc2e169a9a9e85"
						+ "3a14000c4e8d301ab4158f92ddb7d420aad7b979d438b"
						+ "7b61b5bd888bc8803a0050af936b7f6c3e370" + "\n" +
						"58aa10a7f2b6a94f691da25a3faeb0f8f2b3d369f13b0d9a"
						+ "b1c53a3b68334a0cbcff990a75f7bf72cdc2e408272ec063a42"
						+ "e29b2662e4e884aa907e7c5d88dd0");
				String taxiId = sc.next();
				//Memoria y tiempo
				long memoryBeforeCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				long startTime = System.nanoTime();

				//Cargar data
				Controller.loadServices( taxiId );

				//Tiempo en cargar
				long endTime = System.nanoTime();
				long duration = (endTime - startTime)/(1000000);

				//Memoria usada
				long memoryAfterCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				System.out.println("Tiempo en cargar: " + duration + " milisegundos \nMemoria utilizada:  "+ ((memoryAfterCase1 - memoryBeforeCase1)/1000000.0) + " MB");

				break;
			case 2:
				int [] resultadoOrdenInv = Controller.servicesInInverseOrder();

				System.out.println("Total de servicios en orden inverso de tiempo de inicio "+ resultadoOrdenInv[0]);
				System.out.println("Total de servicios que No estan en orden inverso de tiempo de inicio "+ resultadoOrdenInv[1]);

				break;
			case 3:
				int [] resultadoOrden = Controller.servicesInOrder();

				System.out.println("Total de servicios en orden de tiempo de inicio "+ resultadoOrden[0]);
				System.out.println("Total de servicios que No estan en orden de tiempo de inicio "+ resultadoOrden[1]);

				break;
			case 4:
				int [] resultadoOrden2 = Controller.servicesInOrder2();

				System.out.println("Total de servicios en orden de tiempo de inicio globalmente"+ resultadoOrden2[0]);
				System.out.println("Total de servicios que No estan en orden de tiempo de inicio "+ resultadoOrden2[1]);

				break;
			case 5:	
				fin=true;
				sc.close();
				break;
			}
		}
	}

	private static void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 3----------------------");
		System.out.println("1. Cargar los servicios de un taxi reportados en un subconjunto de datos");
		System.out.println("2. Verificar el ordenamiento inverso de los servicios del taxi por tiempo de inicio");
		System.out.println("3. Verificar el ordenamiento de los servicios del taxi por tiempo de inicio");
		System.out.println("4. Verificar el ordenamiento de los servicios del taxi por tiempo de inicio globalmente(opcion 2)");
		System.out.println("5. Salir");
		System.out.println("Digite el n�mero de opci�n para ejecutar la tarea, luego presione enter: (Ej., 1):");

	}
}
