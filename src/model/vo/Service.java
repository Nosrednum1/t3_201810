package model.vo;

/**
 * Representation of a Service object
 */
public class Service implements Comparable<Service> {


	private String trip_id;

	private String taxi_id;

	private String trip_start_timestamp;

	private double trip_miles;

	private double trip_total;

	private int trip_seconds;

	//	public Service(String idtrip, String idtaxi, String timeStart,
	//			double miles, double total, int seconds){
	//		taxi_id = idtaxi;		trip_miles = miles;
	//		trip_id = idtrip;		trip_total = total;
	//		trip_start_timestamp = timeStart;	trip_seconds = seconds;
	//	}

	/**
	 * @return id - Trip_id
	 */
	public String getTripId() {
		// TODO Auto-generated method stub
		return trip_id;
	}	

	/**
	 * @return id - Taxi_id
	 */
	public String getTaxiId() {
		// TODO Auto-generated method stub
		return taxi_id;
	}	

	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() {
		// TODO Auto-generated method stub
		return trip_seconds;
	}

	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public double getTripMiles() {
		// TODO Auto-generated method stub
		return trip_miles;
	}

	/**
	 * @return total - Total cost of the trip
	 */
	public double getTripTotal() {
		// TODO Auto-generated method stub
		return trip_total;
	}

	/**
	 * @return total - Total cost of the trip
	 */
	public String getTripStartTime() {
		// TODO Auto-generated method stub
		return trip_start_timestamp;
	}

	@Override
	public int compareTo(Service o) {
		String[] MyDate = this.trip_start_timestamp.split("T");
		String[] Date = o.getTripStartTime().split("T");
		return (MyDate[1].compareTo(Date[1])>0)?1:-1;
	}
}
