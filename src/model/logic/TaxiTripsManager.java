package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Stack;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import api.ITaxiTripsManager;
import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.Queue;
import model.vo.Taxi;
import model.vo.Creator;
import model.vo.Service;

public class TaxiTripsManager implements ITaxiTripsManager {

	// TODO
	// Definition of data model 
	private Queue<Service> servicesQueue;
	private Stack<Service> servicesStack;

	public void loadServices(String serviceFile, String taxiId) {
		// TODO Auto-generated method stub
		servicesQueue = new Queue<Service>();
		servicesStack = new Stack<Service>();
		System.out.println("Inside loadServices with File:" + serviceFile);
		System.out.println("Inside loadServices with TaxiId:" + taxiId);
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(new File(serviceFile)));
			Gson gson = new GsonBuilder().create();
			Service[] services = gson.fromJson(reader, Service[].class);
			//			System.out.println(services[0].getTaxiId());
			//			System.out.println(services[1].getTaxiId());
			//			System.out.println(services[2].getTaxiId());
			//			System.out.println(services[3].getTaxiId());
			for (Service servicio : services){
				if(servicio.getTaxiId().startsWith(taxiId)){
					System.out.println(servicio.getTripStartTime() );
					servicesQueue.enqueue(servicio);
					servicesStack.add(servicio);
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
			System.out.println("Error al cargar el archivo");}
	}

	@Override
	public int [] servicesInInverseOrder() {
		// TODO Auto-generated method stub
		System.out.println("Inside servicesInInverseOrder");
		int [] resultado = new int[2];
		return resultado;
	}

	/**
	 * @author Anderson Barragan Agudelo
	 * @return arreglo con la cantidad de <b> <u>PARES</u> consecutivos sin 
	 * eliminar intermediarios no ordenados</b> o sea -> [b,c,a,d]:: b,c -- a,d ::: 2
	 */
	@Override
	public int [] servicesInOrder() {
		Queue<Service> servicios = servicesQueue;
		int [] resultado = new int[2];
		System.out.println("Inside servicesInGlobalOrder");
		Service a = null;
		Service b = null;
		while(!servicios.isEmpty()){
			if(a == null)a = servicios.dequeue();
			else a = b;	
			b = servicios.dequeue();
			if(b != null){
				if(a.compareTo(b) < 0)resultado[0]++;
				else resultado[1]++;
			}
		}
		return resultado;
	}

	/**
	 * @author Anderson Barragan Agudelo
	 * @return arreglo con la cantidad de <b> consecutivos globalmente 
	 * eliminando intermediarios no ordenados</b> o sea -> [a,d,b,c,e]:: a,d,e ::: 3
	 */
	@Override
	public int [] servicesInOrder2() {
		// TODO Auto-generated method stub
		Queue<Service> ss = servicesQueue;
		int [] resultado = new int[2];
		System.out.println("Inside servicesInGlobalOrder");
		Service temp = null;
		Service s = null;
		while(!ss.isEmpty()){
			if(temp == null)
				temp = ss.dequeue();
			else temp = s;
			s = ss.dequeue();
			if(s != null){
				if(temp.compareTo(s) > 0){
					while(s != null && temp.compareTo(s) > 0){
						resultado[1]++;
						s = ss.dequeue();
					}
					resultado[0]++;
				}
				else resultado[0]++;
			}
		}
		return resultado;
	}

}
