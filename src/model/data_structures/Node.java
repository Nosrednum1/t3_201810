package model.data_structures;

/**
 * @author Anderson Barragan Agudelo
 * @param <T> Dato gen�rico
 */
public class Node <T extends Comparable<T>>{

	private Node<T> next;

	private Node<T> prev;

	private T MyInfo;

	public Node(T item){
		MyInfo = item; next = prev = null;
	}

	public T getItem(){
		return MyInfo;
	}

	public void setNext(Node<T> next){
		this.next = next;
	}

	public Node<T> next(){
		return next;
	}
	public void setPrev(Node<T> prev){
		this.prev = prev;
	}

	public Node<T> prev(){
		return prev;
	}

}
