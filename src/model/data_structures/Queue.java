package model.data_structures;

/**
 * @author Anderson Barragan Agudelo
 * @param <T> Dato gen�rico
 */
public class Queue<T extends Comparable<T>> implements IQueue<T>{

	private Node<T> first;
	private Node<T> last;

	@Override
	public void enqueue(T item) {
		Node<T> n = new Node<T>(item);
		if(first == null)
			first = last = n;
		else{n.setNext(first);
		first.setPrev(n);
		first = n;}}

	@Override
	public T dequeue() {
		T deq = null;
		if(last != null){
			deq = last.getItem();
			if(last == first)
				first = last = null;
			else{last = last.prev();
			last.setNext(null);}}
		return deq;}

	@Override
	public boolean isEmpty(){return (first == null);}
}
